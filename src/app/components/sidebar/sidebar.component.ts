import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../login/login.service';
import {SessionService} from '../../utils/services/session.service';
import {ToastService} from '../../utils/services/toast.service';
import {Router} from '@angular/router';

declare const $: any;

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES: any[] = [
    {path: '/dashboard', title: 'Início', icon: 'home', class: '', permission: 'all'},
    {path: '/exercicios', title: 'Exercícios', icon: 'fitness_center', class: '', permission: 'normal'},
    {path: '/perfil-usuario', title: 'Perfil do Usuário', icon: 'person', class: '', permission: 'normal'},
    {path: '/listar-usuarios', title: 'Listar Usuários', icon: 'format_list_bulleted', class: '', permission: 'admin'},
    {path: '/registrar-usuario', title: 'Registrar Usuário', icon: 'person_add', class: '', permission: 'admin'},
    {path: '/realizar-treino', title: 'Realizar Treino', icon: 'fitness_center', class: '', permission: 'admin'},
    {path: '/adicionar-treino', title: 'Adicionar Treino', icon: 'note_add', class: '', permission: 'admin'}
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    menuItems: any[];
    user;

    constructor(private sessionService: SessionService,
                private loginService: LoginService,
                private toastService: ToastService,
                private router: Router) {
        this.user = this.sessionService.getAuthenticatedUser();
    }

    ngOnInit() {
        this.menuItems = ROUTES;
    }

    signOut() {
        if(this.sessionService.isUserAuthenticated()) {
            this.loginService.logout().then((result) => {
                this.sessionService.clearAuthenticatedUser();
                this.toastService.showSuccess('Logout realizado com sucesso!');
                this.router.navigate(['/login']);
            }).catch((error) => {
                this.toastService.showSuccess('Não conseguimos finalizar a operação. Tente novamente!');
            });
        }
    }

    isMobileMenu() {
        return $(window).width() <= 991;
    };
}
