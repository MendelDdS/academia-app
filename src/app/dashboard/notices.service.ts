import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFirestore} from 'angularfire2/firestore';

@Injectable()
export class NoticesService {

    constructor(private afAuth: AngularFireAuth,
                private afDatabase: AngularFireDatabase,
                private db: AngularFirestore) {
    }

    addNotice(notice) {
        if (notice) {
            return this.db.collection('avisos').add(notice).then((response) => {
                notice.id = response.id;
                return this.db.collection('avisos').doc(response.id).set(notice);
            });
        }

        return null;
    }

    getNotices() {
        return this.db.collection('avisos').valueChanges();
    }

    removeNotice(notice) {
        return this.db.collection('avisos').doc(notice.id).delete();
    }
}
