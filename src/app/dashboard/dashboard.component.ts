import {Component, OnInit} from '@angular/core';
import {SessionService} from '../utils/services/session.service';
import {AuthService} from '../utils/services/auth.service';
import {NgForm} from '@angular/forms';
import {NoticesService} from './notices.service';
import * as moment from 'moment';
import {PagerService} from '../utils/services/pager.service';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {ConfirmDialogComponent} from './confirm-dialog/confirm-dialog.component';
import {ToastService} from '../utils/services/toast.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    user;
    notices = [];

    pager: any = {};
    noticesPagedItems: any[];

    loading;

    constructor(private sessionService: SessionService,
                private authService: AuthService,
                private noticesService: NoticesService,
                private toastService: ToastService,
                private pagerService: PagerService,
                private dialog: MatDialog) {
        this.noticesPagedItems = [];
    }

    ngOnInit() {
        this.user = this.sessionService.getAuthenticatedUser();
        this.getAllNotices();
        this.loading = true;
    }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        // get pager object from service
        this.pager = this.pagerService.getPager(this.notices.length, page);

        // get current page of items
        this.noticesPagedItems = this.notices.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    addNotice(notice: NgForm) {
        this.loading = true;
        let now = moment();

        const noticeToSave = {
            noticeTitle: notice.controls.noticeTitle.value,
            noticeDescription: notice.controls.noticeDescription.value,
            time: now.format('DD/MM/YYYY - HH:mm')
        };

        notice.onReset();

        this.noticesService.addNotice(noticeToSave).then((response) => {
            this.loading = false;
            if (this.notices.length <= 4) {
                this.noticesPagedItems = this.notices;
            }
        });
    }

    getAllNotices() {
        this.noticesService.getNotices().subscribe((response) => {
            this.setNotices(response);
            this.loading = false;
        });
    }

    setNotices(notices: any[]) {
        this.notices = notices;
        this.setPage(1);
    }

    removeNotice(notice) {
        this.noticesService.removeNotice(notice).then((response) => {
            if (this.notices.length <= 4) {
                this.noticesPagedItems = this.notices;
            }
            this.toastService.showSuccess("Aviso removido com sucesso.");
        }).catch((err) => {
            this.toastService.showError('Tivemos algum problema remoção do aviso. Tente novamente!');
        });
    }

    openDialog(notice) {
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;

        this.dialog.open(ConfirmDialogComponent, dialogConfig)
            .afterClosed()
            .subscribe((result) => {
                if (result) {
                    this.removeNotice(notice);
                }
            });
    }

    print() {
        let now = moment();
        let perimetersContent = document.getElementById('perimeters').innerHTML;
        let neuromotorsContent = document.getElementById('neuromotors').innerHTML;
        let disp_setting = "toolbar=yes,location=no,";
        disp_setting += "directories=yes,menubar=yes,";
        disp_setting += "scrollbars=yes,width=650, height=600, left=100, top=25";
        let docprint = window.open("", "", disp_setting);
        docprint.document.write(`
        <!DOCTYPE html>
        <html lang="en">
        
        <head>
            <meta charset="utf-8">
            <title></title>
        
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">
        
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">

            <style>@page { size: A5 landscape }</style>
        
            <style>
                body   { font-family: serif }
                h1     { font-family: 'Tangerine', cursive; font-size: 40pt; line-height: 18mm}
                h2 { font-size: 18pt; line-height: 7mm }
                h3 { font-size: 14pt; line-height: 7mm }
                li     { font-size: 11pt; line-height: 5mm }
                h1      { margin: 0 }
                h1 + ul { margin: 2mm 0 5mm }
                h2, h3  { margin: 0 3mm 3mm 0; float: left }
                h4      { margin: 2mm 0 0 50mm; border-bottom: 2px solid black }
                h4 + ul { margin: 5mm 0 0 50mm }
                article { border: 4px double black; padding: 5mm 10mm; border-radius: 3mm }
                table {
                    border-collapse: collapse;
                }
                p { margin: 0 }

                th, td {     
                    border-bottom: 1px solid black;
                }
            </style>
            <script type="text/javascript">
                setTimeout(function () { window.print(); }, 500);
                window.onfocus = function () { setTimeout(function () { window.close(); }, 500); }
            </script>
        </head>
        
        <body class="A5">

        <section class="sheet padding-20mm">`);
        docprint.document.write('<div><p>Aluno: ' + this.user.firstname + ' ' + this.user.lastname + '</p></div>');
        docprint.document.write('<div><p>Data da Avaliação: ' + now.format('DD/MM/YYYY - HH:mm:ss') + '</p></div>');
        docprint.document.write(`
            </br>
            <p><strong>AVALIAÇÃO DE PERIMETROS</strong></p>

            <table>
                <thead>
                    <th></th>
                    <th>Direito(a)</th>
                    <th></th>
                    <th>Esquerdo(a)</th>
                </thead>
                <tbody>
                <tr>
                                    <td>
                                        Braço relaxado
                                    </td>
                                    <td>
                                        <p>` + this.user.perimeters.relaxArmRight+ `cm</p>
                                    </td>
                                    <td></td>
                                    <td>
                                        <p class="text-right">` + this.user.perimeters.relaxArmLeft+ `cm</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Braço contraído
                                    </td>
                                    <td>
                                        <p>` + this.user.perimeters.contractedArmRight+ `cm</p>
                                    </td>
                                    <td></td>
                                    <td>
                                        <p class="text-right">` + this.user.perimeters.contractedArmLeft+ `cm</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Antebraço
                                    </td>
                                    <td>
                                        <p>` + this.user.perimeters.forearmRight+ `cm</p>
                                    </td>
                                    <td></td>
                                    <td>
                                        <p class="text-right">` + this.user.perimeters.forearmLeft+ `cm</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Tórax relaxado
                                    </td>
                                    <td></td>
                                    <td>
                                        <p>` + this.user.perimeters.relaxThorax+ `cm</p>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        Cintura
                                    </td>
                                    <td></td>
                                    <td>
                                        <p>` + this.user.perimeters.waist+ `cm</p>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        Abdômen
                                    </td>
                                    <td></td>
                                    <td>
                                        <p>` + this.user.perimeters.abdmone+ `cm</p>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        Quadril
                                    </td>
                                    <td></td>
                                    <td>
                                        <p>` + this.user.perimeters.hip + `cm</p>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        Coxa
                                    </td>
                                    <td>
                                        <p>` + this.user.perimeters.thighRight+ `cm</p>
                                    </td>
                                    <td></td>
                                    <td>
                                        <p class="text-right">` + this.user.perimeters.thighLeft+ `cm</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Panturrilha
                                    </td>
                                    <td>
                                        <p>` + this.user.perimeters.calfRight + `cm</p>
                                    </td>
                                    <td></td>
                                    <td>
                                        <p class="text-right">` + this.user.perimeters.calfRight + `cm</p>
                                    </td>
                                </tr>
                </tbody>
            </table>`);

        docprint.document.write(`
                </br>
                <p><strong>AVALIAÇÃO DOS NEUROMOTORES</strong></p>
                <p>Flexão de Braços: </h4>` + this.user.neuromotors.pushUps + `</p></br>
                <p><= 16 Ruim</p>
                <p>17 - 27 Abaixo da Média</p>
                <p>22 - 28 Média</p>
                <p>29 - 35 Acima da Média</p>
                <p>>= 36 Excelente</p></br>
                <p>Abdominal: </h4>` + this.user.neuromotors.abdominal + `</p></br>
                <p><= 28 Ruim</p>
                <p>29 - 32 Abaixo da Média</p>
                <p>33 - 36 Média</p>
                <p>37 - 42 Acima da Média</p>
                <p>>= 43 Excelente</p>`);
        docprint.document.write(`
         </section>
        
        </body>
        
        </html>`);
        docprint.document.close();
        docprint.focus();
    }

    isAdmin() {
        return this.authService.isAdmin();
    }

}
