import {NgModule} from '@angular/core';
import {CommonModule,} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {Routes, RouterModule} from '@angular/router';

import {UserLayoutComponent} from './layouts/user-layout/user-layout.component';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {LoginGuard} from './utils/guards/login.guard';
import {RedirectGuard} from './utils/guards/redirect.guard';
import {FirstChangePasswordComponent} from './first-change-password/first-change-password.component';
import {FirstAccessGuard} from './utils/guards/first-access.guard';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
        canActivate: [RedirectGuard]
    },
    {
        path: 'primeiro-acesso',
        component: FirstChangePasswordComponent,
        canActivate: [FirstAccessGuard]
    },
    {
        path: '',
        component: UserLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './layouts/user-layout/user-layout.module#UserLayoutModule'
            }]
    },
    {
        path: 'login',
        component: LoginComponent,
        canActivate: [LoginGuard]
    },
    {
        path: 'home',
        component: HomeComponent,
        canActivate: [RedirectGuard]
    },
    {
        path: '**',
        redirectTo: 'dashboard',
        canActivate: [RedirectGuard]
    }
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    exports: [],
})
export class AppRoutingModule {
}
