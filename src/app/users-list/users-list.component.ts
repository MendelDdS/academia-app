import {Component, OnInit} from '@angular/core';
import {UsersListService} from './users-list.service';

@Component({
    selector: 'app-users-list',
    templateUrl: './users-list.component.html',
    styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
    users;
    loading;
    number;

    constructor(private usersListService: UsersListService) {

    }

    ngOnInit() {
        this.loading = true;
        this.getAllProjects();
    }

    getAllProjects() {
        this.usersListService.getAllUsers().subscribe((users) => {
            this.users = users;
            this.loading = false;
        });
    }

}
