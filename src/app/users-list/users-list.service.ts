import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFirestore} from 'angularfire2/firestore';

@Injectable()
export class UsersListService {

    constructor(private afAuth: AngularFireAuth,
                private afDatabase: AngularFireDatabase,
                private db: AngularFirestore) {
    }

    getAllUsers() {
        return this.db
            .collection('usuarios', ref => ref.where('type', '==', 'normal'))
            .valueChanges();
    }

}
