import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FooterComponent} from '../components/footer/footer.component';
import {NavbarComponent} from '../components/navbar/navbar.component';
import {SidebarComponent} from '../components/sidebar/sidebar.component';
import {UserLayoutModule} from '../layouts/user-layout/user-layout.module';

@NgModule({
    imports: [
        CommonModule,
        UserLayoutModule
    ],
    declarations: [
        // FooterComponent,
        // NavbarComponent,
        // SidebarComponent
    ]
})
export class HomeModule {
}
