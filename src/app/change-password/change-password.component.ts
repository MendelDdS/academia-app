import {Component, OnInit} from '@angular/core';
import {FormGroup, NgForm} from '@angular/forms';
import {ChangePasswordService} from '../utils/services/change-password.service';
import {UserProfileService} from '../user-profile/user-profile.service';
import {Router} from '@angular/router';
import {SessionService} from '../utils/services/session.service';
import {ToastService} from '../utils/services/toast.service';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
    user;

    constructor(private changePasswordService: ChangePasswordService,
                private userProfileService: UserProfileService,
                private sessionService: SessionService,
                private toastService: ToastService,
                private router: Router) {
    }

    ngOnInit() {
        this.user = this.sessionService.getAuthenticatedUser();
    }

    changePassword(passwordForm: NgForm) {
        if (passwordForm.value.newPassword !== passwordForm.value.repeatPassword) {
            this.toastService.showError('As senhas não conferem!');
        } else {
            this.changePasswordService
                .changePassword(passwordForm.value.newPassword, passwordForm.value.currentPassword)
                .then((result) => {
                    let userUpdate = this.user;
                    userUpdate.firstTime = false;
                    this.toastService.showSuccess("Senha alterada com sucesso.");
                    this.userProfileService.update(userUpdate).then((result) => {
                        this.router.navigate(['perfil-usuario']);
                        this.sessionService.setUser(userUpdate);
                    }).catch((error) => {
                        this.toastService.showError("Tivemos um problema. Melhor tentar novamente!");
                    });
                }).catch((error) => {
                    this.toastService.showWarning('Parece que a senha atual está incorrenta.');
            });
        }
    }

}
