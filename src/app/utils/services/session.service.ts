import { Injectable } from '@angular/core';
import {LocalStorageService} from "ngx-webstorage";
import {CookieOptions, CookieService} from "ngx-cookie";

@Injectable()
export class SessionService {

    private cookieOptions: CookieOptions;
    private dataExpires: Date;

    constructor(private localSt: LocalStorageService, private cookieService: CookieService) { }

    cookieDateExpires() {
        const TIME_COOKIE_HOUR = 1;
        this.dataExpires = new Date();
        this.dataExpires.setHours(this.dataExpires.getHours() + TIME_COOKIE_HOUR);
        this.cookieOptions = {expires: this.dataExpires} as CookieOptions;
        return this.cookieOptions;
    }


    setUserAuthenticated(user, token) {
        this.localSt.store('user', btoa(JSON.stringify(user)));
        this.localSt.store('authenticated', btoa('true'));
        this.cookieService.put('academia-user-token', token, this.cookieDateExpires());
    }


    isUserAuthenticated() {
        const authenticated = atob(this.localSt.retrieve('authenticated'));
        return authenticated === 'true' && (this.getUserToken() !== undefined && this.getUserToken() !== null);
    }


    clearAuthenticatedUser() {
        this.localSt.clear('user');
        this.localSt.clear('authenticated');
        localStorage.removeItem('firebase:host:academia-campestre.firebaseio.com');
        window.location.reload();
        this.cookieService.remove('academia-user-token');
        this.localSt.store('authenticated', btoa('false'));
    }


    getAuthenticatedUser() {
        if(this.isUserAuthenticated()) {
            return JSON.parse(atob(this.localSt.retrieve('user')));
        } return undefined;
    }

    getUserToken() {
        return this.cookieService.get('academia-user-token');
    }

    setUser(user) {
        this.localSt.store('user', btoa(JSON.stringify(user)));
    }

    isUserLogged() {
        const authenticated = atob(this.localSt.retrieve('authenticated'));
        return authenticated === 'true';
    }

}
