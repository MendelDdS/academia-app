import {Injectable} from '@angular/core';
import {SessionService} from "./session.service";

@Injectable()
export class AuthService {
    constructor(private sessionService: SessionService) {

    }

    isAdmin() {
        return this.sessionService.isUserAuthenticated() && this.sessionService.getAuthenticatedUser()["type"] === 'admin';
    }

    isStudent() {
        return this.sessionService.isUserAuthenticated() && this.sessionService.getAuthenticatedUser()["type"] === 'normal';
    }

    isFirstAccess() {
        return this.sessionService.isUserAuthenticated() && this.sessionService.getAuthenticatedUser()["firstTime"];
    }
}
