import {Injectable} from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFirestore} from 'angularfire2/firestore';
import {AngularFireDatabase} from 'angularfire2/database';
import * as firebase from 'firebase/app';
import * as admin from 'firebase-admin';

@Injectable()
export class ChangePasswordService {

    constructor(private afAuth: AngularFireAuth,
                private afDatabase: AngularFireDatabase,
                private db: AngularFirestore,) {
    }

    changePassword(newPassword, currentPassword) {
        return this.reauthenticate(currentPassword).then(() => {
            return this.afAuth.auth.currentUser.updatePassword(newPassword);
        });
    }

    reauthenticate = (currentPassword) => {
        let user = firebase.auth().currentUser;
        let cred = firebase.auth.EmailAuthProvider.credential(
            user.email, currentPassword);
        return user.reauthenticateAndRetrieveDataWithCredential(cred);
    }
}
