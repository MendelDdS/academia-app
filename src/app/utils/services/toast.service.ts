import { Injectable , ViewContainerRef} from '@angular/core';
import {ToastOptions, ToastsManager} from 'ng2-toastr';

@Injectable()
export class ToastService {

    constructor(private toastr: ToastsManager, private toastOpts: ToastOptions) {
        this.toastOpts.showCloseButton = true;
    }

    showSuccess(text) {
        this.toastr.success(text, 'Sucesso!', this.toastOpts);
    }

    showError(text) {
        this.toastr.error(text, 'Oops!', this.toastOpts);
    }
    showWarning(text) {
        this.toastr.warning(text, 'Alerta!', this.toastOpts);
    }
    showInfo(text) {
        this.toastr.info(text);
    }
    showCustom(text) {
        this.toastr.custom('<span style="color: red">Message in red.</span>', null, {enableHTML: true});
    }
}
