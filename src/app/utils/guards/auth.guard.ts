import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route} from '@angular/router';
import { Observable } from 'rxjs';
import {SessionService} from '../services/session.service';

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {

    constructor(private router: Router, private sessionService: SessionService) {}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.canPass();
    }

    canPass() {

        if (this.sessionService.isUserAuthenticated()) {
            return true;
        }

        this.router.navigate(['login']);

        return false;
    }

    canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
        return this.canPass();
    }




}
