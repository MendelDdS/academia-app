import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Route, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {SessionService} from '../services/session.service';

@Injectable()
export class LoginGuard implements CanActivate {

    constructor(private router: Router, private sessionService: SessionService) {

    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.canPass();
    }

    canPass() {
        if (this.sessionService.isUserAuthenticated()) {
            this.router.navigate(['dashboard']);
            return false;
        }

        return true;
    }

    canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
        return this.canPass();
    }
}
