import { TestBed, async, inject } from '@angular/core/testing';

import { FirstAccessGuard } from './first-access.guard';

describe('FirstAccessGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FirstAccessGuard]
    });
  });

  it('should ...', inject([FirstAccessGuard], (guard: FirstAccessGuard) => {
    expect(guard).toBeTruthy();
  }));
});
