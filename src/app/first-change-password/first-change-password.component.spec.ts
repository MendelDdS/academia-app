import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstChangePasswordComponent } from './first-change-password.component';

describe('FirstChangePasswordComponent', () => {
  let component: FirstChangePasswordComponent;
  let fixture: ComponentFixture<FirstChangePasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstChangePasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstChangePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
