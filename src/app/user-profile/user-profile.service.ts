import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFirestore} from 'angularfire2/firestore';

@Injectable()
export class UserProfileService {


    constructor(private afAuth: AngularFireAuth,
                private afDatabase: AngularFireDatabase,
                private db: AngularFirestore) {
    }

    update(user) {
        return this.db.collection('usuarios').doc(user.uid).update(user);
    }

    getUpdatedUser(user) {
        return this.db.collection('usuarios').doc(user.uid).valueChanges();
    }

}
