import {Component, OnInit} from '@angular/core';
import {UserProfileService} from './user-profile.service';
import {LoginService} from '../login/login.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SessionService} from '../utils/services/session.service';
import {Router} from '@angular/router';
import {ToastService} from '../utils/services/toast.service';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

    profileForm: FormGroup;
    user;

    constructor(private sessionService: SessionService,
                private formBuilder: FormBuilder,
                private userProfileService: UserProfileService,
                private router: Router,
                private toastService: ToastService) {
    }

    ngOnInit() {
        this.user = this.sessionService.getAuthenticatedUser();
        this.initForm();
        this.initializeForm(this.user);
    }

    goChangePassword() {
        this.router.navigate(['mudar-senha']);
    }

    initForm() {
        this.profileForm = this.formBuilder.group({
            'firstname': [null, Validators.required],
            'lastname': [null, Validators.required],
            'address': [null, Validators.required],
            'email': [null, Validators.required],
            'description': [null, Validators.required]
        });
    }

    initializeForm(user) {
        this.profileForm = this.formBuilder.group({
            'firstname': [user.firstname, Validators.required],
            'lastname': [user.lastname, Validators.required],
            'address': [user.address, Validators.required],
            'email': [{value: user.email, disabled: true}],
            'description': [user.description]
        });
    }

    formIsValid() {
        return this.profileForm.valid;
    }

    updateProfile(profileData) {
        profileData.uid = this.user.uid;
        profileData.email = this.user.email;
        profileData.type = this.user.type;
        profileData.gender = this.user.gender;
        this.userProfileService.update(profileData).then((result) => {
            this.sessionService.setUser(profileData);
            this.toastService.showSuccess('Dados atualizado com sucesso!');
            this.router.navigate(['dashboard']);
        }).catch((error) => {
            this.toastService.showError('Tivemos um problema ao atualizar seus dados. Tente Novamente!');
        });
    }
}
