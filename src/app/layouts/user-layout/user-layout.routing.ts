import {Routes} from '@angular/router';

import {DashboardComponent} from '../../dashboard/dashboard.component';
import {UserProfileComponent} from '../../user-profile/user-profile.component';
import {TableListComponent} from '../../table-list/table-list.component';
import {TypographyComponent} from '../../typography/typography.component';
import {IconsComponent} from '../../icons/icons.component';
import {MapsComponent} from '../../maps/maps.component';
import {NotificationsComponent} from '../../notifications/notifications.component';
import {UpgradeComponent} from '../../upgrade/upgrade.component';
import {RegisterUserComponent} from '../../register-user/register-user.component';
import {ExerciseComponent} from '../../exercise/exercise.component';
import {AdminGuard} from '../../utils/guards/admin.guard';
import {AuthGuard} from '../../utils/guards/auth.guard';
import {StudentGuard} from '../../utils/guards/student.guard';
import {UsersListComponent} from '../../users-list/users-list.component';
import {ChangePasswordComponent} from '../../change-password/change-password.component';
import {RegisterExerciseComponent} from '../../register-exercise/register-exercise.component';
import {AddTrainingComponent} from '../../add-training/add-training.component';

export const UserLayoutRoutes: Routes = [
    {path: 'table-list', component: TableListComponent},
    {path: 'typography', component: TypographyComponent},
    {path: 'icons', component: IconsComponent},
    {path: 'maps', component: MapsComponent},
    {path: 'notifications', component: NotificationsComponent},
    {path: 'upgrade', component: UpgradeComponent},
    {
        path: 'registrar-usuario',
        component: RegisterUserComponent,
        canActivate: [AuthGuard, AdminGuard],
        canLoad: [AuthGuard, AdminGuard]
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'exercicios',
        component: ExerciseComponent,
        canActivate: [AuthGuard, StudentGuard],
        canLoad: [AuthGuard, StudentGuard]
    },
    {
        path: 'perfil-usuario',
        component: UserProfileComponent,
        canActivate: [AuthGuard, StudentGuard],
        canLoad: [AuthGuard, StudentGuard]
    },
    {
        path: 'listar-usuarios',
        component: UsersListComponent,
        canActivate: [AuthGuard, AdminGuard],
        canLoad: [AuthGuard, AdminGuard]
    },
    {
        path: 'mudar-senha',
        component: ChangePasswordComponent,
        canActivate: [AuthGuard],
        canLoad: [AuthGuard]
    },
    {
        path: 'realizar-treino',
        component: RegisterExerciseComponent,
        canActivate: [AuthGuard, AdminGuard],
        canLoad: [AuthGuard, AdminGuard]
    },
    {
        path: 'adicionar-treino',
        component: AddTrainingComponent,
        canActivate: [AuthGuard, AdminGuard],
        canLoad: [AuthGuard, AdminGuard]
    }
];
