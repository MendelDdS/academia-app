import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DashboardComponent} from '../../dashboard/dashboard.component';
import {UserProfileComponent} from '../../user-profile/user-profile.component';
import {TableListComponent} from '../../table-list/table-list.component';
import {TypographyComponent} from '../../typography/typography.component';
import {IconsComponent} from '../../icons/icons.component';
import {MapsComponent} from '../../maps/maps.component';
import {NotificationsComponent} from '../../notifications/notifications.component';
import {UpgradeComponent} from '../../upgrade/upgrade.component';

import {
    MatButtonModule,
    MatInputModule, MatProgressSpinnerModule,
    MatRippleModule,
    MatTooltipModule,
    MatSelectModule,
    MatOptionModule,
    MatDividerModule,
    MatDialogModule
} from '@angular/material';
import {RegisterUserComponent} from '../../register-user/register-user.component';
import {UserLayoutRoutes} from './user-layout.routing';
import {ExerciseComponent} from '../../exercise/exercise.component';
import {UsersListComponent} from '../../users-list/users-list.component';
import {ChangePasswordComponent} from '../../change-password/change-password.component';
import {RegisterExerciseComponent} from '../../register-exercise/register-exercise.component';
import {AddTrainingComponent} from '../../add-training/add-training.component';
import {ConfirmDialogComponent} from '../../dashboard/confirm-dialog/confirm-dialog.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(UserLayoutRoutes),
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatRippleModule,
        MatInputModule,
        MatSelectModule,
        MatDividerModule,
        MatOptionModule,
        MatTooltipModule,
        MatProgressSpinnerModule,
        MatDialogModule
    ],
    declarations: [
        TableListComponent,
        TypographyComponent,
        IconsComponent,
        MapsComponent,
        NotificationsComponent,
        UpgradeComponent,
        RegisterUserComponent,
        DashboardComponent,
        UserProfileComponent,
        ExerciseComponent,
        UsersListComponent,
        ChangePasswordComponent,
        RegisterExerciseComponent,
        AddTrainingComponent,
        ConfirmDialogComponent
    ],
    entryComponents: [
        ConfirmDialogComponent
    ]
})

export class UserLayoutModule {
}
