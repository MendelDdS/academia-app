import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, FormsModule, NgForm, Validators} from '@angular/forms';
import {RegisterUserService} from './register-user.service';
import {ToastService} from '../utils/services/toast.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-register-user',
    templateUrl: './register-user.component.html',
    styleUrls: ['./register-user.component.scss']
})
export class RegisterUserComponent implements OnInit {

    userForm: FormGroup;

    constructor(private registerUserService: RegisterUserService,
                private toastService: ToastService,
                private formBuilder: FormBuilder,
                private router: Router) {
    }

    ngOnInit() {
        this.initUserForm();
    }

    initUserForm() {
        this.userForm = this.formBuilder.group({
            'emailControl': [null, Validators.required],
            'firstnameControl': [null, Validators.required],
            'lastnameControl': [null, Validators.required],
            'genderControl': [null, Validators.required],
            'typeControl': [null, Validators.required],
            'relaxArmRightControl': [0, Validators.required],
            'relaxArmLeftControl': [0, Validators.required],
            'contractedArmRightControl': [0, Validators.required],
            'contractedArmLeftControl': [0, Validators.required],
            'forearmRightControl': [0, Validators.required],
            'forearmLeftControl': [0, Validators.required],
            'relaxThoraxControl': [0, Validators.required],
            'waistControl': [0, Validators.required],
            'abdmoneControl': [0, Validators.required],
            'hipControl': [0, Validators.required],
            'thighRightControl': [0, Validators.required],
            'thighLeftControl': [0, Validators.required],
            'calfRightControl': [0, Validators.required],
            'calfLeftControl': [0, Validators.required],
            'pushUpsControl':[0, Validators.required],
            'abdominalControl': [0, Validators.required]
        });
    }

    createUser(data) {
        let user;
        user = {
            email: data.emailControl,
            firstname: data.firstnameControl,
            lastname: data.lastnameControl,
            gender: data.genderControl,
            type: data.typeControl,
            firstTime: true
        };

        if (data.typeControl === 'normal') {
            const perimeters = {
                relaxArmRight: data.relaxArmRightControl,
                relaxArmLeft: data.relaxArmLeftControl,
                contractedArmRight: data.contractedArmRightControl,
                contractedArmLeft: data.contractedArmLeftControl,
                forearmRight: data.forearmRightControl,
                forearmLeft: data.forearmLeftControl,
                relaxThorax: data.relaxThoraxControl,
                waist: data.waistControl,
                abdmone: data.abdmoneControl,
                hip: data.hipControl,
                thighRight: data.thighRightControl,
                thighLeft: data.thighLeftControl,
                calfRight: data.calfRightControl,
                calfLeft: data.calfLeftControl
            };

            const neuromotors = {
                pushUps: data.pushUpsControl,
                abdominal: data.abdominalControl
            };

            user.perimeters = perimeters;
            user.neuromotors = neuromotors;

        }

        this.registerUserService.createUser(user).then((response) => {
            this.toastService.showSuccess('Usuário criado com sucesso.');
            this.router.navigate(['dashboard']);
        }).catch((error) => {
            if (error.code === 'auth/email-already-in-use') {
                this.toastService.showWarning('Este e-mail já está em uso!');
            } else {
                this.toastService.showError('Ocorreu algum erro e não conseguimos criar a conta.');
            }
        });
    }
}
