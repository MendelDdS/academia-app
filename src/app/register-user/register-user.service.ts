import {Injectable} from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFirestore} from 'angularfire2/firestore';
import * as firebase from 'firebase/app';

@Injectable()

export class RegisterUserService {
    PASSWORD = 'USUARIO123';

    constructor(private afAuth: AngularFireAuth,
                private afDatabase: AngularFireDatabase,
                private db: AngularFirestore) {
    }

    createUser(user) {
        let config = {apiKey: "AIzaSyA6u3rVsrzoW2m0EE8B4Ib_KlCvgdLqkG8",
            authDomain: "academia-campestre.firebaseapp.com",
            databaseURL: "https://academia-campestre.firebaseio.com",
            projectId: "academia-campestre",
            storageBucket: "academia-campestre.appspot.com",
            messagingSenderId: "758802254083"};

        let secondaryApp = firebase.initializeApp(config, "Secondary");

        return secondaryApp.auth().createUserWithEmailAndPassword(user.email, this.PASSWORD)
            .then((response) => {
                this.associateDataUser(user, response.user.uid).then((response) => {
                    secondaryApp.auth().signOut();
                });

            });
    }

    associateDataUser(user, uid) {
        user.uid = uid;
        return this.db.collection('usuarios').doc(uid).set(user);
    }

}
