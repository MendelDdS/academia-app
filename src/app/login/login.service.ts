import {Injectable} from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import {Router} from '@angular/router';
import {AngularFireDatabase} from 'angularfire2/database';
import {AngularFirestore} from 'angularfire2/firestore';

@Injectable()
export class LoginService {

    userData = null;

    constructor(private afAuth: AngularFireAuth,
                private afDatabase: AngularFireDatabase,
                private db: AngularFirestore,
                private router: Router) {

    }

    authenticate(email, password) {
        return this.afAuth.auth.signInWithEmailAndPassword(email, password);
    }

    retrieveDataUser(callback) {
        this.afAuth.auth.onAuthStateChanged((userLogged) => {
            if (userLogged) {
                this.db.collection('usuarios', ref => ref.where('uid', '==', userLogged.uid))
                    .valueChanges()
                    .subscribe(callback);
            }
        });
    }

    navigate(user) {
        if (user.firstTime) {
            this.router.navigate(['primeiro-acesso']);
        } else {
            this.router.navigate(['dashboard']);
        }
    }

    getCurrentUser() {
        return this.userData;
    }

    logout() {
       return this.afAuth.auth.signOut();
    }
}
