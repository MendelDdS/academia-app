import {Component, OnInit} from '@angular/core';
import {FormsModule, NgForm} from '@angular/forms';
import {LoginService} from './login.service';
import {Router} from '@angular/router';
import {SessionService} from '../utils/services/session.service';
import {ToastService} from '../utils/services/toast.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loading;
    clickRegister;

    constructor(private loginService: LoginService,
                private router: Router,
                private sessionService: SessionService,
                private toastService: ToastService) {
    }

    ngOnInit() {
        this.clickRegister = true;
    }

    clickingRegister() {
        this.clickRegister = false;
    }

    authenticate(data: NgForm) {
        if (!data.valid) {
            return;
        }

        this.loading = true;

        this.loginService.authenticate(data.controls.email.value, data.controls.password.value)
            .then((response) => {
                this.loginService.retrieveDataUser((user) => {
                    this.loading = false;
                    this.sessionService.setUserAuthenticated(user[0], response.user.refreshToken);
                    this.loginService.navigate(user[0]);
                });
            })
            .then(() => {
                this.toastService.showSuccess('Usuário autenticado com sucesso!');
            })
            .catch((error) => {
            this.loading = false;
            this.toastService.showError('A senha está inválida ou o usuário não existe!');
        });
    }
}
