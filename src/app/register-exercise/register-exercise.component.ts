import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {TrainingService} from '../add-training/training.service';
import {UsersListService} from '../users-list/users-list.service';
import {FormControl, Validators} from '@angular/forms';
import {UserProfileService} from '../user-profile/user-profile.service';
import {ToastService} from '../utils/services/toast.service';
import {Router} from '@angular/router';
import * as moment from 'moment';

@Component({
    selector: 'app-register-exercise',
    templateUrl: './register-exercise.component.html',
    styleUrls: ['./register-exercise.component.scss']
})
export class RegisterExerciseComponent implements OnInit {

    trainings;
    users;
    trainingForm: FormGroup;

    userControl = new FormControl('', [Validators.required]);


    constructor(private trainingService: TrainingService,
                private userService: UsersListService,
                private userProfileService: UserProfileService,
                private formBuilder: FormBuilder,
                private router: Router,
                private toastService: ToastService) {
    }

    ngOnInit() {
        this.initTrainingForm();
        this.getTrainings();
        this.getAllUsers();
    }

    initTrainingForm() {
        this.trainingForm = this.formBuilder.group({
            'userControl': [null, Validators.required],
            'trainingControl': [null, Validators.required],
        });
    }

    addUserTraining(trainingForm) {
        let training = trainingForm.trainingControl;
        let now = moment();
        training.createDate = now.format('DD/MM/YYYY - HH:mm:ss');
        training.dueDate = now.add(1, 'day').format('DD/MM/YYYY - HH:mm:ss');
        let user =  trainingForm.userControl;

        user.training = training;

        this.userProfileService.update(user).then((response) => {
            this.toastService.showSuccess('Dados atualizado com sucesso!');
            this.router.navigate(['dashboard']);
        }).catch((error) => {
            this.toastService.showError('Tivemos um problema ao atualizar seus dados. Tente Novamente!');
        });
    }

    getTrainings() {
        this.trainingService.getTrainings().subscribe((trainings) => {
            this.trainings = trainings;
        });
    }

    getAllUsers() {
        this.userService.getAllUsers().subscribe((users) => {
          this.users = users;
        })
    }

}
