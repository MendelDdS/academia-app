import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {AngularFirestore} from 'angularfire2/firestore';
import {AngularFireAuth} from 'angularfire2/auth';

@Injectable()
export class TrainingService {

    constructor(private afAuth: AngularFireAuth,
                private afDatabase: AngularFireDatabase,
                private db: AngularFirestore) {
    }

    createTraining(training) {
        return this.db.collection('treinos').add(training);
    }

    getTrainings() {
        return this.db.collection('treinos').valueChanges();
    }
}
