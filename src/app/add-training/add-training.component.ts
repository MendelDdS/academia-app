import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {TrainingService} from './training.service';
import {ToastService} from '../utils/services/toast.service';
import {Router} from '@angular/router';
import {SessionService} from '../utils/services/session.service';

@Component({
    selector: 'app-add-exercise',
    templateUrl: './add-training.component.html',
    styleUrls: ['./add-training.component.scss']
})
export class AddTrainingComponent implements OnInit {

    exercises = [];
    user;

    constructor(private sessionService: SessionService,
                private trainingService: TrainingService,
                private toastService: ToastService,
                private router: Router) {
    }

    ngOnInit() {
        this.user = this.sessionService.getAuthenticatedUser();
    }


    createTraining(trainingData: NgForm) {
        const training = {
            name: trainingData.controls.name.value,
            trainer: this.user.firstname + ' ' + this.user.lastname,
            objective: trainingData.controls.objective.value,
            comments:  trainingData.controls.comments.value,
            exercises: this.exercises
        };

        this.trainingService.createTraining(training).then((response) => {
            this.router.navigate(['dashboard']);
            this.toastService.showSuccess('Treino criado com sucesso.');
        }).catch((error) => {
            this.toastService.showError('Tivemos algum problema na criação do treino. Tente novamente!');
        });
    }

    addExercise(exerciseData: NgForm) {

        const exercise = {
            exerciseName: exerciseData.controls.exerciseName.value,
            serie: exerciseData.controls.serie.value,
            repetitions: exerciseData.controls.repetitions.value,
        };

        this.exercises.push(exercise);
        exerciseData.onReset();
    }

}
