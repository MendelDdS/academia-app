import {Component, OnInit} from '@angular/core';
import {SessionService} from '../utils/services/session.service';
import {UserProfileService} from '../user-profile/user-profile.service';
import {ToastService} from '../utils/services/toast.service';
import {Router} from '@angular/router';
import * as moment from 'moment';

@Component({
    selector: 'app-exercise',
    templateUrl: './exercise.component.html',
    styleUrls: ['./exercise.component.scss']
})
export class ExerciseComponent implements OnInit {

    user;

    constructor(private sessionService: SessionService,
                private userProfileService: UserProfileService,
                private toastService: ToastService,
                private router: Router) {
    }

    ngOnInit() {
        this.getUser();
        this.verifyTraining();
    }

    verifyTraining() {

    }

    getUser() {
        this.user = this.sessionService.getAuthenticatedUser();

        this.userProfileService.getUpdatedUser(this.user).subscribe((user) => {
            this.sessionService.setUser(user);
            this.user = user;
        });
    }

    finishExercises() {
        let userUpdated = this.user;
        userUpdated.training = null;

        this.userProfileService.update(userUpdated).then((user) => {
            this.sessionService.setUser(userUpdated);
            this.toastService.showSuccess('Exercícios finalizados!');
            this.router.navigate(['dashboard']);
        }).catch((error) => {
            this.toastService.showError('Ocorreu algum problema, tente novamente!');
        });
    }

    print() {
        let now = moment();

        let disp_setting = "toolbar=yes,location=no,";
        disp_setting += "directories=yes,menubar=yes,";
        disp_setting += "scrollbars=yes,width=650, height=600, left=100, top=25";
        let docprint = window.open("", "", disp_setting);
        docprint.document.write(`
        <!DOCTYPE html>
        <html lang="en">
        
        <head>
            <meta charset="utf-8">
            <title></title>
        
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">
        
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">

            <style>@page { size: A5 landscape }</style>
        
            <link href='https://fonts.googleapis.com/css?family=Tangerine:700' rel='stylesheet' type='text/css'>
            <style>
                body   { font-family: serif }
                h1     { font-family: 'Tangerine', cursive; font-size: 40pt; line-height: 18mm}
                h2, h3 { font-family: 'Tangerine', cursive; font-size: 24pt; line-height: 7mm }
                h4     { font-size: 32pt; line-height: 14mm }
                h2 + p { font-size: 18pt; line-height: 7mm }
                h3 + p { font-size: 14pt; line-height: 7mm }
                li     { font-size: 11pt; line-height: 5mm }
                h1      { margin: 0 }
                h1 + ul { margin: 2mm 0 5mm }
                h2, h3  { margin: 0 3mm 3mm 0; float: left }
                h4      { margin: 2mm 0 0 50mm; border-bottom: 2px solid black }
                h4 + ul { margin: 5mm 0 0 50mm }
                article { border: 4px double black; padding: 5mm 10mm; border-radius: 3mm }
                table {
                    border-collapse: collapse;
                }
                p { margin: 0 }

                th, td {     
                    border-bottom: 1px solid black;
                }
            </style>
            <script type="text/javascript">
                setTimeout(function () { window.print(); }, 500);
                window.onfocus = function () { setTimeout(function () { window.close(); }, 500); }
            </script>
        </head>
        
        <body class="A5 landscape">

        <section class="sheet padding-20mm">`);

        docprint.document.write(`
                <p><strong>ALUNO:</strong> ` + this.user.firstname + ' ' + this.user.lastname + `</p>
                <p><strong>TREINO:</strong> ` + this.user.training.name + `</p>
                <p><strong>PROFESSOR:</strong> ` + this.user.training.trainer + `</p>
                <p><strong>OBJETIVO:</strong> ` + this.user.training.objective + `</p>
                <p><strong>OBSERVAÇÕES:</strong>` + this.user.training.comments + `</p>
                <p><strong>DATA DA CRIAÇÃO:</strong> ` + this.user.training.createDate +`</p>
                <p><strong>DATA DE VENCIMENTO:</strong> ` + this.user.training.dueDate + `</p></br></br>`);

        docprint.document.write(`
                    <p align="center">---------------------------------------</p>
                    <p align="center">FICHA DE TREINO</p>
                    <p align="center">--` + this.user.training.objective +`--</p>`);

        docprint.document.write(`
            <table>
                <thead>
                    <th style="padding-right: 40px">EXERCÍCIO</th>
                    <th>SÉRIE</th>
                    <th>REPET</th>
                </thead>
                <tbody>`);

        for (let exercise of this.user.training.exercises) {
            docprint.document.write(`<tr>`);
            docprint.document.write(`<td style="padding-right: 40px">` + exercise.exerciseName + `</td>`);
            docprint.document.write(`<td align="center" style="border-left: 1px solid black;
                                                        border-right: 1px solid black;">` + exercise.serie + `</td>`);
            docprint.document.write(`<td align="center">` + exercise.repetitions + `</td>`);
            docprint.document.write(`</tr>`);
        }

        docprint.document.write(`
                </tbody>
            </table>
        
        </section>
        
        </body>
        
        </html>
        `);
        docprint.document.close();
        docprint.focus();
    }
}
