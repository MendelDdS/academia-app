import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';


import {AppRoutingModule} from './app.routing';
import {ComponentsModule} from './components/components.module';

import {AppComponent} from './app.component';

import {UserLayoutComponent} from './layouts/user-layout/user-layout.component';
import {LoginComponent} from './login/login.component';
import {
    MatButtonModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatRippleModule,
    MatTooltipModule
} from '@angular/material';

import { Ng2Webstorage } from 'ngx-webstorage';
import { CookieModule } from 'ngx-cookie';

import { AngularFireModule } from 'angularfire2';
import { FirebaseConfig } from './../environments/firebase.config';
import {BrowserModule} from '@angular/platform-browser';
import { AngularFireDatabase } from 'angularfire2/database';
import {RegisterUserService} from './register-user/register-user.service';
import {AngularFireAuth} from 'angularfire2/auth';
import {LoginService} from './login/login.service';
import {UserProfileService} from './user-profile/user-profile.service';
import {SessionService} from './utils/services/session.service';
import {HomeComponent} from './home/home.component';
import {AuthGuard} from './utils/guards/auth.guard';
import {LoginGuard} from './utils/guards/login.guard';
import {AuthService} from './utils/services/auth.service';
import {AdminGuard} from './utils/guards/admin.guard';
import {StudentGuard} from './utils/guards/student.guard';
import {UsersListService} from './users-list/users-list.service';
import {AngularFirestore, AngularFirestoreModule} from 'angularfire2/firestore';
import { FirstChangePasswordComponent } from './first-change-password/first-change-password.component';
import {ChangePasswordService} from './utils/services/change-password.service';
import {FirstAccessGuard} from './utils/guards/first-access.guard';
import {ToastService} from './utils/services/toast.service';
import {ToastModule} from 'ng2-toastr';
import {TrainingService} from './add-training/training.service';
import {NoticesService} from './dashboard/notices.service';
import {PagerService} from './utils/services/pager.service';

@NgModule({
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        ComponentsModule,
        RouterModule,
        AppRoutingModule,
        MatButtonModule,
        MatRippleModule,
        MatInputModule,
        MatTooltipModule,
        MatProgressSpinnerModule,
        AngularFireModule.initializeApp(FirebaseConfig),
        AngularFirestoreModule,
        ReactiveFormsModule,
        ToastModule.forRoot(),
        Ng2Webstorage.forRoot({ prefix: 'academia', separator: '-', caseSensitive: true }),
        CookieModule.forRoot()
    ],
    declarations: [
        AppComponent,
        UserLayoutComponent,
        LoginComponent,
        HomeComponent,
        FirstChangePasswordComponent
    ],
    providers: [
        AngularFireDatabase,
        AngularFireAuth,
        AngularFirestore,
        AuthGuard,
        LoginGuard,
        StudentGuard,
        AdminGuard,
        LoginService,
        UserProfileService,
        RegisterUserService,
        SessionService,
        AuthService,
        UsersListService,
        ChangePasswordService,
        ToastService,
        FirstAccessGuard,
        TrainingService,
        NoticesService,
        PagerService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
